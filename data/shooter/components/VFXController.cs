using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "0d37cd3d2effad786f409277eab0cd6aa89b9dc1")]
public class VFXController : Component
{
	[ParameterFile(Filter = ".node")]
	public string hitPrefab = null;

	[ParameterFile(Filter = ".node")]
	public string muzzleFlashPrefab = null;

	public void OnShoot(mat4 transform)
	{
		if (string.IsNullOrEmpty(hitPrefab))
			return;

		Node muzzleFlashVFX = World.LoadNode(muzzleFlashPrefab);
		muzzleFlashVFX.WorldTransform = transform;
	}

	public void OnHit(vec3 hitPoint, vec3 hitNormal, Unigine.Object hitObject)
	{
		if (string.IsNullOrEmpty(hitPrefab))
			return;

		Node hitVFX = World.LoadNode(hitPrefab);
		hitVFX.WorldPosition = hitPoint;
		hitVFX.SetWorldDirection(hitNormal, vec3.UP, MathLib.AXIS.Y);
	}
}
